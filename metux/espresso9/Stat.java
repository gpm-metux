package metux.espresso9;

public class Stat
{
    public String name;
    public String uid;
    public String gid;
    long   mode;
    long   size;
    long   atime;
    long   mtime;
    long   ctime;
}
