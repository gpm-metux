package metux.espresso9;

public class File
{
    private long fd;

    public static final int ReadOnly   = NativeFS.omode_ReadOnly;
    public static final int WriteOnly  = NativeFS.omode_WriteOnly;
    public static final int ReadWrite  = NativeFS.omode_ReadWrite;
    public static final int AutoCreate = NativeFS.omode_Create;

    public File(String name, int mode)
    {
	fd = NativeFS.open(name, mode);
	if (fd<0)
	    throw new RuntimeException("Cannot open file: \""+name+"\" mode="+mode);
    }

    private File(long newfd)
    {
	fd = newfd;
    }

    public byte[] read(long size)
    {
	return NativeFS.read(fd, size);
    }

    public long write(byte[] data)
    {
	return NativeFS.write(fd, data);
    }

    public void finalize()
	throws Throwable
    {
	NativeFS.close(fd);
	super.finalize();
    }

    public Stat stat()
    {
	return NativeFS.fstat(fd);
    }

    public Stat scan()
    {
	return NativeFS.scan(fd);
    }

    public File lookup(String name)
    {
	long newfd = NativeFS.lookup(fd, name);
	if (newfd<0)
	    throw new RuntimeException("lookup failed for "+name);
	return new File(newfd);
    }

    public void reset()
    {
	NativeFS.reset(fd);
    }
}
