package metux.espresso9;

public class NativeFS
{
    public static final int err_FileNotFound     = -1;
    public static final int err_PermissionDenied = -2;
    public static final int err_TooManyOpenFiles = -3;

    public static final int omode_ReadOnly  = 1;
    public static final int omode_WriteOnly = 2;
    public static final int omode_ReadWrite = 3;
    public static final int omode_RWMASK    = 15;
    public static final int omode_Create    = 16;

    private static boolean initialized = false;
    private static native long   n_open(String name, int mode);
    private static native byte[] n_read(long fd, long size);
    private static native byte[] n_pread(long fd, long size, long offset);
    private static native long   n_write(long fd, byte[] data);
    private static native long   n_pwrite(long fd, byte[] data, long offset);
    private static native long   n_close(long fd);
    private static native Stat   n_fstat(long fd, Stat st);
    private static native Stat   n_scan(long fd, Stat st);
    private static native void   n_reset(long fd);
    private static native long   n_lookup(long fd, String name);

    private static void initialize()
    {
	if (initialized)
	    return;
	System.loadLibrary("java-rlt-espresso9");
	initialized = true;
    }

    public static long open(String name, int mode)
    {
	initialize();
	return n_open(name, mode);
    }
    public static byte[] read(long fd, long size)
    {
	initialize();
	return n_read(fd, size);
    }
    public static byte[] pread(long fd, long size, long offset)
    {
	initialize();
	return n_pread(fd, size, offset);
    }
    public static long write(long fd, byte[] data)
    {
	initialize();
	return n_write(fd, data);
    }
    public static long pwrite(long fd, byte[] data, long offset)
    {
	initialize();
	return n_pwrite(fd, data, offset);
    }
    public static long close(long fd)
    {
	initialize();
	return n_close(fd);
    }
    public static Stat fstat(long fd)
    {
	initialize();
	return n_fstat(fd, new Stat());
    }
    public static Stat scan(long fd)
    {
	initialize();
	return n_scan(fd, new Stat());
    }
    public static void reset(long fd)
    {
	initialize();
	n_reset(fd);
    }
    public static long lookup(long fd, String name)
    {
	initialize();
	return n_lookup(fd, name);
    }
}
