package de.metux.espresso.test;

import de.metux.espresso.*;
import de.metux.espresso.fs.*;

public class TestClassLoader
{
    public static void test()
    {
	HostFile host = HostFile.createRoot("/home/crosstool/src/espresso9/trunk/classes");
	testClass(host, "de.metux.espresso.test.inter");
	testClass(host, "de.metux.espresso.test.dummy");
    }

    public static void testClass(IFile fs, String classname)
    {	
	EspressoClassLoader cl = new EspressoClassLoader(fs);
	Class c;
	try
	{
	    c = cl.findClass(classname);
	    if (c == null)
	    {
		System.err.println("TestClassLoader: Could not load class");
		return;
	    }
	}
	catch (ClassNotFoundException e)
	{
	    System.err.println("TestClassLoader: Class not found :(");
	    System.err.println(e);
	    return;
	}

	System.err.println("Successfully loaded class: "+c.getName());

/*	try
	{
	    inter i = (inter)c.newInstance();
	} catch (InstantiationException e)
	{
	    System.err.println("could instantiate class "+c.getName());
	} catch (IllegalAccessException e)
	{
	    System.err.println("illegal access on "+c.getName());
	}
*/
    }    
}
