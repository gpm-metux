
package de.metux.espresso.fs;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface IFile
{
	public	byte[] read(long size)			// may return shorted than size, even file is long enough
	    throws FileNotFoundException, IOException;

	public	byte[] read_p(long size, long pos);

	public	long   write(byte[] data);
	public	long   write(byte[] data, long size, long offset);

	public	long   write_p(byte[] data, long pos);
	public	long   write_p(byte[] data, long pos, long size, long offset);

	public	Stat   stat();
	public  IFile  lookup(String filename);

	public  Stat    scan();		// iterate through directories
	public  boolean reset();	// reset iteration

	public  boolean abort();		// abort the current request (from another thread)

	public  IFile	cloneFile();
}
