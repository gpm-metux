package de.metux.espresso.fs;

public class Stat
{
	public String name;
	public String uid;
	public String gid;
	
	public int    mode;
	public long   size;
	public long   mtime;
	public long   atime;
	public long   ctime;
}
