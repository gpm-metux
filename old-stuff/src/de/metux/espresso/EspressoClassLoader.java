package de.metux.espresso;

import de.metux.espresso.fs.IFile;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;

public class EspressoClassLoader	extends java.lang.ClassLoader
{
	private IFile root;
	private Hashtable loaded = new Hashtable();
	
	public EspressoClassLoader(IFile f)
	{
		root = f;
	}

	public byte[] loadClassData(String name)
	    throws ClassNotFoundException
	{
		if (name == null)
		    throw new ClassNotFoundException("null classname");

		String filename = name.replace('.','/')+".class";

		Espresso.logInfo("Loading class: "+name+" ("+filename+")");

		IFile classfile = root.lookup(filename);
		if (classfile == null)
		    throw new ClassNotFoundException("Missing classfile: "+filename);

		try
		{
			byte[] data = classfile.read(65535);
			if (data == null)
				throw new ClassNotFoundException("empty class file: "+filename);
			return data;
		} catch (FileNotFoundException e)
		{
			throw new ClassNotFoundException("Class file not found: "+filename);
		} catch (IOException e)
		{
			throw new ClassNotFoundException("Class file cannot be read: "+filename);
		}
	}

	/* only let's pass certain classes that directly belong to the java language and intrinsic utils */
	public boolean isCoreClass(String name)
	{
	    if (name.equals("java.lang.Object")  		|| 
	        name.equals("java.lang.Integer") 		||
		name.equals("java.lang.String")  		||
		name.equals("java.lang.Boolean") 		||
		name.equals("java.util.Hashtable") 		||
		name.equals("de.metux.espresso.fs.IFile") 	||
//		name.equals("de.metux.espresso.test.inter")	||
		name.equals("de.metux.espresso.fs.Stat"))
		    return true;
	    
	    return false;
	}

	/* is this an environment specific class (eg. what java.lang.System normally is to java apps) */ 
	public boolean isEnvClass(String name)
	{
		if (name.equals("de.metux.espresso.Espresso"))
			return true;
		return false;
	}

	public Class loadClass(String name)
	    throws ClassNotFoundException
	{
		Class cls;
		
		Espresso.logInfo("loadClass: name="+name);
	
		if (isCoreClass(name))
		{
			Espresso.logInfo("letting core class pass: "+name);
			cls = getParent().loadClass(name);
			if (cls == null)
			    throw new ClassNotFoundException("parent class loader could not load core class: "+name);
			return cls;
		}

		if (isEnvClass(name))
		{
			Espresso.logInfo("ups, env classes not handled yet");
			throw new ClassNotFoundException("env classes not handled yet: "+name);
		}

		/* try to get an class already loaded by us */
		if ((cls = (Class)loaded.get(name)) != null)
		{
			Espresso.logInfo("sending already-loaded local class: "+name);
			return cls;
		}

		cls = findClass(name);

//		byte[] b = loadClassData(name);
//		cls = defineClass(name, b, 0, b.length);
		if (cls == null)
			throw new ClassNotFoundException("findClass() retrieved null class: "+name);
		
		resolveClass(cls);
		loaded.put(name, cls);

		Espresso.logInfo("adding class: "+cls.getName());
		return cls;
	}

	public Class findClass(String name)
		throws ClassNotFoundException
	{
		Espresso.logInfo("findClass() name="+name);
		byte[] b = loadClassData(name);
		return defineClass(name, b, 0, b.length);
//		return loadClass(name);
	}

/*	
	public void resolveClass(Class c)
	{
		System.err.println("ResolveClass(): "+c.getName());
		super.resolveClass(c);
	}
*/
}
