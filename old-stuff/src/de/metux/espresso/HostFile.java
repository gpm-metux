package de.metux.espresso;

import de.metux.espresso.fs.*;
import java.io.*;

public class HostFile implements IFile
{
	private String  root;
	private boolean isdir;
	private File    filehandle;
	private FileInputStream in;

	private HostFile()
	{
	}

	public static HostFile createRoot(String r)
	{
		HostFile hf = new HostFile();
		hf.root       = r;
		hf.isdir      = true;
		hf.filehandle = new File(r);
		return hf;
	}

	public static HostFile createDir(String r, File fh)
	{
		HostFile hf = new HostFile();
		hf.root       = r;
		hf.isdir      = true;
		hf.filehandle = fh;
		return hf;
	}

	public static HostFile createPlain(String r, File fh)
        {
		HostFile hf = new HostFile();
		hf.root       = r;
		hf.isdir      = false;
		hf.filehandle = fh;
		return hf;    
	}

	private void __input()
		throws FileNotFoundException
	{
		if (in == null)
		    in = new FileInputStream(filehandle);
	}
    
	public byte[] read(long size)
		throws FileNotFoundException, IOException
	{
		__input();
		int sz = in.available();
		if (sz > size)
		    sz = (int)size;
		byte[] buf = new byte[sz];
		in.read(buf);
		return buf;
	}

	public byte[] read_p(long size, long offset)
	{
		return null;
	}

	public long write(byte[] buf)
	{
		return -1;
	}

	public long write(byte[] buf, long size, long offset)
	{
		return -1;
	}

	public long write_p(byte[] data, long pos)
	{
		return -1;
	}

	public long write_p(byte[] data, long pos, long size, long offset)
    	{
		return -1;
	}

	public Stat stat()
        {
		return null;
	}

	public IFile lookup(String name)
	{
		String fullname = root+"/"+name;
		Espresso.logInfo("lookup: "+fullname);
		File fh = new File(fullname);
		HostFile newfile = new HostFile();

		if (fh.isDirectory())
			return createDir(root, fh);
		else if (fh.exists())
			return createPlain(root, fh);

	    return null;
	}

	public Stat scan()
	{
		return null;
	}
    
	public boolean reset()
	{
		return false;
	}
    
	public boolean abort()
	{
		return false;
	}
    
	public IFile cloneFile()
	{
		return null;
	}
}
