
#include <stdio.h>

#include <mvfs/mvfs.h>
#include <mvfs/autoconnect_ops.h>

#include "metux_espresso9_NativeFS.h"

#define MAX_FILES 	64

static MVFS_FILESYSTEM* fs = NULL;
static MVFS_FILE* files[MAX_FILES];

static void init_filesystems()
{
    if (fs != NULL)
	return;

    memset(files, 0, sizeof(files));
    fs = mvfs_autoconnectfs_create();
}

static int addfile(MVFS_FILE* f)
{
    if (f==NULL)
	return -1;

    int x;
    for (x=0; x<MAX_FILES; x++)
    {
	if (files[x] == NULL)
	{
	    files[x] = f;
	    return x;
	}
    }

    fprintf(stderr,"metux.espresso9.NativeFilesystem: addfile() no more slots. bailing out\n");
    return -1;
}

static MVFS_FILE* getfile(int fd)
{
    if (fd<MAX_FILES)
    {
	if (!files[fd])
	{
	    fprintf(stderr,"metux.espresso9.NativeFS: getfile() fd not open %d\n", fd);
	    return NULL;
	}
	return files[fd];
    }
    fprintf(stderr,"metux.espresso9.NativeFS: getfile() fd %d out of range\n", fd);
    return NULL;
}

JNIEXPORT jlong JNICALL Java_metux_espresso9_NativeFS_n_1open
  (JNIEnv * env, jclass cls, jstring name, jint mode)
{
    const jbyte* str = (*env)->GetStringUTFChars(env, name, NULL);
    if (str == NULL)
	return -1;	/* OOM */

    init_filesystems();

    int m;
    switch (mode & metux_espresso9_NativeFS_omode_RWMASK)
    {
	case metux_espresso9_NativeFS_omode_ReadOnly:	m = O_RDONLY;	break;
	case metux_espresso9_NativeFS_omode_WriteOnly:	m = O_WRONLY;	break;
	case metux_espresso9_NativeFS_omode_ReadWrite:	m = O_RDWR;	break;
	default: m = mode;
    }

    if (mode & metux_espresso9_NativeFS_omode_Create)
	m |= O_CREAT;

    MVFS_FILE* f = mvfs_fs_openfile(fs, str, m);
    if (f==NULL)
    {
	fprintf(stderr,"metux.espresso9.NativeFS::open() could not open file\n");
	return -1;
    }

    (*env)->ReleaseStringUTFChars(env, name, str);
    return addfile(f);
}

JNIEXPORT jbyteArray JNICALL Java_metux_espresso9_NativeFS_n_1read
  (JNIEnv * env, jclass cls, jlong fd, jlong size)
{
    init_filesystems();

    // we should throw an exception ;-o
    MVFS_FILE* f = getfile(fd);
    if (f==NULL)
    {
	fprintf(stderr,"metux.espresso9.NativeFS::read(fd,pos,size): file not open: %ld\n", fd);
	return NULL;
    }
    if (size<1)
    {
	fprintf(stderr,"metux.espresso9.NativeFS::read(fd,pos,size). size out of range %ld\n", size);
	return NULL;
    }

    char* buffer = alloca(size);
    long readsize = mvfs_file_read(f, buffer, size);

    if (readsize==0)
	return NULL;

    if (readsize<0)
    {
	fprintf(stderr,"metux.espresso.NativeFS::read(fd,pos,size) error %s\n", strerror(f->errcode));
	// should throw an exception
	return NULL;
    }

    jbyteArray ret = (*env)->NewByteArray(env, readsize);
    (*env)->SetByteArrayRegion(env, ret, 0, readsize, (jbyte *)buffer);

    return ret;
}

JNIEXPORT jlong JNICALL Java_metux_espresso9_NativeFS_n_1write
  (JNIEnv *env, jclass cls, jlong fd, jbyteArray data)
{
    init_filesystems();

    // we should throw an exception ;-o
    MVFS_FILE* f = getfile(fd);
    if (f==NULL)
    {
	fprintf(stderr,"metux.espresso9.NativeFS::read(fd,data[]): file not open: %ld\n", fd);
	return -1;
    }

    if (data==NULL)
	return 0;

    long size = (*env)->GetArrayLength(env, data);

    jbyte* buffer = alloca(size);
    (*env)->GetByteArrayRegion(env, data, 0, size, (jbyte*)buffer);
    return mvfs_file_write(f, buffer, size);
}

JNIEXPORT jlong JNICALL Java_metux_espresso9_NativeFS_n_1close
  (JNIEnv *env, jclass cls, jlong fd)
{
    init_filesystems();

    MVFS_FILE* f = getfile(fd);
    if (f==NULL)
	return -1;

    mvfs_file_close(f);
    files[fd] = NULL;

    return 0;
}

static jobject alloc_stat(JNIEnv* env, MVFS_STAT* st)
{
    jclass cls = (*env)->FindClass(env, "metux.espresso9.Stat");
    if (cls==NULL)
    {
	fprintf(stderr, "alloc_stat() class lookup failed\n");
	return NULL;
    }

    jobject stat = (*env)->AllocObject(env, cls);
    if (stat == NULL)
    {
	fprintf(stderr,"alloc_stat() object creation failed\n");
	return NULL;
    }

    return stat;
}

static void set_obj_string(JNIEnv* env, jclass cls, jobject obj, const char* name, const char* value)
{
    if (value==NULL)
    {
	fprintf(stderr,"set_obj_string() NULL value\n");
	return;
    }
    jfieldID field = (*env)->GetFieldID(env, cls, name, "Ljava/lang/String;");
    if (field == NULL)
    {
	fprintf(stderr,"cannot get field ID for \"%s\"\n", name);
	return;
    }
    jobject of = (*env)->NewStringUTF(env,value);
    if (of == NULL)
    {
	fprintf(stderr,"cannot create java string for \"%s\" -> \"%s\"\n", name, value);
	return;
    }
    (*env)->SetObjectField(env, obj, field, of);
}

static jobject stat_to_java(JNIEnv* env, MVFS_STAT* st, jobject stobj)
{
    if (st == NULL)
	return NULL;

    jclass cls = (*env)->GetObjectClass(env, stobj);
    if (cls == NULL)
    {
	fprintf(stderr,"stat_to_java() cannot get return object class\n");
	return NULL;
    }
    set_obj_string(env, cls, stobj, "name", st->name);
    set_obj_string(env, cls, stobj, "uid",  st->uid);
    set_obj_string(env, cls, stobj, "gid",  st->gid);
    return stobj;
}

JNIEXPORT jobject JNICALL Java_metux_espresso9_NativeFS_n_1fstat
  (JNIEnv *env, jclass myclass, jlong fd, jobject jst)
{
    MVFS_FILE* f = getfile(fd);
    if (f == NULL)
    {
	fprintf(stderr,"fstat() illegal fd passed: %d\n", fd);
	return NULL;
    }

    MVFS_STAT* ust = mvfs_file_stat(f);
    return stat_to_java(env, ust, jst);
}

JNIEXPORT jobject JNICALL Java_metux_espresso9_NativeFS_n_1scan
  (JNIEnv *env, jclass myclass, jlong fd, jobject jst)
{
    MVFS_FILE* f = getfile(fd);
    if (f == NULL)
    {
	fprintf(stderr,"scan() illegal fd passed: %d\n", fd);
	return NULL;
    }

    MVFS_STAT* ust = mvfs_file_scan(f);
    return stat_to_java(env, ust, jst);
}

JNIEXPORT jlong JNICALL Java_metux_espresso9_NativeFS_n_1lookup
  (JNIEnv *env, jclass myclass, jlong fd, jstring name)
{
    MVFS_FILE* f = getfile(fd);
    if (f == NULL)
    {
	fprintf(stderr,"metux.espresso9.NativeFS::lookup() null file object passed\n");
	return -1;
    }

    const jbyte* str = (*env)->GetStringUTFChars(env, name, NULL);
    if (str == NULL)
	return -1;	/* OOM */

    MVFS_FILE* newfile = mvfs_file_lookup(f, str);
    if (newfile==NULL)
    {
	fprintf(stderr,"metux.espresso9.NativeFS::lookup() could not open file\n");
	return -1;
    }
    fprintf(stderr," -> lookup() oldfd=%ld name=\"%s\" newfd=%ld\n", f, str, newfile);
    (*env)->ReleaseStringUTFChars(env, name, str);
    return addfile(newfile);
}

JNIEXPORT void JNICALL Java_metux_espresso9_NativeFS_n_1reset
  (JNIEnv *env, jclass myclass, jlong fd)
{
    MVFS_FILE* f = getfile(fd);
    if (f == NULL)
    {
	fprintf(stderr,"metux.espresso9.NativeFS::reset() null file object passed\n");
	return;
    }
    mvfs_file_reset(f);
}
