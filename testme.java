import metux.espresso9.NativeFS;
import metux.espresso9.File;
import metux.espresso9.Stat;
import java.lang.Runtime;

public class testme
{
    public static void catfile(File f1, String prefix)
    {
	byte[] data;
	while (((data = f1.read(1024))!=null) && (data.length>0))
	{
	    System.err.print(prefix);
	    for (int l=0; l<data.length; l++)
		System.err.print((char)data[l]);
	}
    }

    public static void main(String par[])
    {
	long fd = NativeFS.open("/etc/passwd", NativeFS.omode_ReadOnly);
	byte[] data;
	long pos = 0;
	File f1 = new File("/etc/passwd", File.ReadOnly);
	File f2 = new File("/tmp/furunkel-001", File.ReadWrite+File.AutoCreate);
	while (((data = f1.read(1024))!=null) && (data.length>0))
	{
	    f2.write(data);
	    for (int l=0; l<data.length; l++)
		System.err.print((char)data[l]);
	    pos += data.length;
	}

	Stat st = f1.stat();
	if (st == null)
	{
	    System.err.println("File::stat() call failed\n");
	}
	else
	{
	    System.err.println("File::stat() name="+st.name+" uid="+st.uid+" gid="+st.gid);
	}

	File f3 = new File("/etc", File.ReadOnly);
	while ((st = f3.scan())!=null)
	{
	    System.err.println("DIRENT: name="+st.name+" uid="+st.uid+" gid="+st.gid);
	    if (st.name.equals("passwd"))
	    {
		System.err.println(" ---> PASSWD");
		File f4 = f3.lookup(st.name);
		catfile(f4, "[passwd] ");
	    }
	}
	f3.reset();
    }
}
