

PKG_CONFIG?=pkg-config
JAVAC?=javac
JAVAH?=javah

MVFS_CFLAGS=`$(PKG_CONFIG) --cflags`
MVFS_LIBS=`$(PKG_CONFIG) --libs`

JAVA_SRCS=\
    metux/espresso9/NativeFS.java	\
    testme.java

JAVA_CLS=\
    classes/metux/espresso9/NativeFS.class	\
    classes/metux/espresso9/File.class		\
    classes/metux/espresso9/Stat.class

NATIVE_CLASSES=\
    metux.espresso9.NativeFS

JNI_SRC=espresso9_jni.c
JNI_OBJ=espresso9_jni.o
JNI_SO=libjava-rlt-espresso9.so
JNI_HDR=metux_espresso9_NativeFS.h

all:	build-classes build-native

build-classes:	clean	$(JAVA_CLS)

$(JAVA_CLS): $(JAVA_SRCS)
	mkdir -p classes/metux/espresso9
	$(JAVAC) -cp . $(JAVA_SRCS)
	mv metux/espresso9/*.class classes/metux/espresso9

metux_espresso9_NativeFS.h:	classes/metux/espresso9/NativeFS.class
	$(JAVAH) -classpath "classes" metux.espresso9.NativeFS

$(JNI_OBJ):	$(JNI_SRC) $(JNI_HDR)
	$(CC) $(MVFS_CFLAGS) -c $(JNI_SRC) -o $(JNI_OBJ)

$(JNI_SO):	$(JNI_OBJ)
	$(LD) -shared $(JNI_OBJ) -o $(JNI_SO) $(MVFS_LIBS)

build-native: $(JNI_OBJ) $(JNI_SO)

clean:
	find -name "*.class" -exec "rm" "{}" ";"
	rm -Rf classes

run:
	java -Djava.library.path=. -classpath "classes:." testme

espresso9.jar:
	jar cvf espresso9.jar mymanifest -C classes
